require('dotenv').config()
const express = require("express");
const path = require("path");
const hbs = require("hbs");
const bcrypt = require("bcryptjs");
const cookieparser = require('cookie-parser');
const auth = require("./middleware/auth")
const app = express();
require("./db/conn");
const User = require("./models/registers");
const { json } = require("express");

const port = process.env.PORT || 3001;
const methodOverride = require("method-override")

// imports router
const products_routes = require("../routes/products");
// route middleware
app.use("/api/products", products_routes);
app.use("/products", products_routes);
const static_path = path.join(__dirname, "../public");
const template_path = path.join(__dirname, "../templates/views");
const partials_path = path.join(__dirname, "../templates/partials");

app.use(express.json());
app.use(cookieparser());
// 
const router = require("../routes/men");
app.use(router);
//

app.use(express.urlencoded({ extended: false }));

app.use(express.static(static_path));
app.set("view engine", "hbs");
app.set("views", template_path);
hbs.registerPartials(partials_path);


//console.log(process.env.SECRET_KEY);

app.get("/", (req, res) => {
    // res.send('This is Demo Login App for Node js')
    // res.render("index")
    res.render("home")

});


app.get("/login", (req, res) => {
        res.render("login");
    })
    //Login
app.post("/login", async(req, res) => {
    try {
        const email = req.body.email;
        const password = req.body.password;
        const useremail = await User.findOne({ email: email });
        const isMatch = await bcrypt.compare(password, useremail.password);
        const token = await useremail.generateAuthToken();
        console.log("the token part" + token);

        res.cookie("jwt", token, {
            expires: new Date(Date.now() + 5000000),
            httpOnly: true
        });
        console.log(` This is the cookie ${req.cookies.jwt}`);
        // if (useremail.password === password) {
        //     res.status(201).render("index");
        //     // res.send(useremail);
        //     // console.log(useremail);

        // } else {
        //     res.send("password are not correct")
        // }
        if (isMatch) {
            res.status(201).render("index");
            // res.send(useremail);
            // console.log(useremail);

        } else {
            res.send("password are not correct")
        }





    } catch (error) {
        res.status(400).send("wrong email or password");
        console.log(error);
    }
})

app.get("/signup", (req, res) => {
        res.render("signup");
    })
    // Signup
app.post("/signup", async(req, res) => {
        try {
            const { userid, name, email, country, password, conform_password } = req.body;

            if (password === conform_password) {
                const signupUser = new User({
                    userid,
                    name,
                    email,
                    country,
                    password,
                    conform_password,
                });
                console.log("signup or Registation Successful")
                console.log("the success part" + signupUser);


                // jwt token
                const token = await signupUser.generateAuthToken();
                console.log("the token part" + token);

                res.cookie("jwt", token, {
                    expires: new Date(Date.now() + 3000000),
                    httpOnly: true
                });
                console.log(cookie);
                const registered = await signupUser.save();
                console.log("the token part" + registered);
                res.redirect("/login")
                    // res.status(201).render("login");
            } else {
                res.render("signup", { error: "password are not matching" });
            }
        } catch (error) {
            res.render("signup", { error: "Enter valid details" });

        }
    })
    //Secrets
app.get("/secrets", auth, (req, res) => {
        console.log(` This is the cookie ${req.cookies.jwt}`);
        res.render("secrets");
    })
    // logout
app.post("/logout", auth, async(req, res) => {
    try {
        console.log(req.user);
        res.clearCookie("jwt");
        console.log("Logout Successfully")

        await req.user.save();
        res.render("login");
    } catch (error) {
        res.status(500).send(error);
    }
})

app.get("/logout", auth, async(req, res) => {
    try {
        console.log(req.user);
        res.clearCookie("jwt");
        console.log("Logout Successfully")

        await req.user.save();
        res.render("login");
    } catch (error) {
        res.status(500).send(error);
    }
})

// JWT

const jwt = require("jsonwebtoken");


// Listning App Port
app.listen(port, () => {
    console.log(`APP listening on port ${port}`);

})
